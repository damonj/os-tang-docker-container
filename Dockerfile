FROM alpine AS build

LABEL maintainer="Michel Belleau <michel.belleau@malaiwah.com>"

RUN apk add --no-cache git g++ musl musl-dev bash gawk gzip make tar gmp mpfr4 mpfr-dev mpc1 mpc1-dev isl isl-dev http-parser-dev autoconf automake gcc make meson ninja openssl-dev jansson-dev zlib-dev

RUN git clone https://github.com/latchset/jose.git && cd jose && meson build && cd build && ninja && ninja install

RUN git clone https://github.com/latchset/tang.git && cd tang && meson build && cd build && ninja && ninja install

FROM alpine
COPY --from=build /usr/local/bin/jose /usr/local/bin/jose
COPY --from=build /usr/local/lib/libjose.so.0 /usr/local/lib/libjose.so.0
COPY --from=build /usr/local/lib/libjose.so.0.0.0 /usr/local/lib/libjose.so.0.0.0

COPY --from=build /usr/libexec/tangd /usr/local/libexec/tangd
COPY --from=build /usr/libexec/tangd-keygen /usr/local/libexec/tangd-keygen
COPY --from=build /usr/libexec/tangd-update /usr/local/libexec/tangd-update

RUN mkdir -p /var/cache/tang && chgrp -R 0 /var/cache/tang && chmod -R g=u /var/cache/tang 
RUN mkdir -p /var/db/tang && chgrp -R 0 /var/db/tang && chmod -R g=u /var/db/tang 

RUN apk add --no-cache bash socat http-parser jansson zlib openssl
EXPOSE 8080
VOLUME /var/db/tang

COPY start.sh /
CMD ["/start.sh"]
